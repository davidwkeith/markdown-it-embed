# Markdown-It Plugin Embed

## Usage
Similar to images, just use and `@` rather than `!` to indicate you want to embed
the URL in an iFrame.

```Markdown
@[A classic rick-roll](https://www.youtube.com/watch?v=dQw4w9WgXcQ "Super Cool Video")
```

The output is a `<figure>` element with an `<iframe>` as the content. By default the figure
has the class name "mardown-it-embed" for styling.
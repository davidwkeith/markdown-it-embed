/* eslint-disable jest/valid-title */
import fs from "fs"
import MarkdownIt from "markdown-it"
import embed from "../src"
import toDiffableHtml from "diffable-html"

/** Read a "fixtures" file, containing a set of tests:
 *
 * test name
 * .
 * input text
 * .
 * expected output
 * .
 *
 * */
function readFixtures(name: string): string[][] {
  const fixtures = fs.readFileSync(`tests/fixtures/${name}.md`).toString()
  return fixtures.split("\n.\n\n").map(s => s.split("\n.\n"))
}

describe("Parses embed markdown for iFrame", () => {
  readFixtures("iframe").forEach(([name, text, expected]) => {
    const mdit = MarkdownIt().use(embed)
    const rendered = mdit.render(text)
    it(name, () => expect(toDiffableHtml(rendered)).toMatch(toDiffableHtml(expected)))
  })
})

describe("Parses embed markdown for Instagram", () => {
  readFixtures("instagram").forEach(([name, text, expected]) => {
    const mdit = MarkdownIt().use(embed)
    const rendered = mdit.render(text)
    it(name, () => expect(toDiffableHtml(rendered)).toMatch(toDiffableHtml(expected)))
  })
})

describe("Parses embed markdown for X (Twitter)", () => {
  readFixtures("x").forEach(([name, text, expected]) => {
    const mdit = MarkdownIt().use(embed)
    const rendered = mdit.render(text)
    it(name, () => expect(toDiffableHtml(rendered)).toMatch(toDiffableHtml(expected)))
  })
})
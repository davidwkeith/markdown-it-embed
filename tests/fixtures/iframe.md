youtube.com iframe
.
@[A classic rick-roll](https://www.youtube.com/embed/dQw4w9WgXcQ "Super Cool Video")
.
<p>
  <figure class="markdown-it-embed">
    <iframe
      src="https://www.youtube.com/embed/dQw4w9WgXcQ"
      title="A classic rick-roll"
      allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
      allowfullscreen
      credentialess
      frameborder="0"
      loading="lazy"
      referrerpolicy="same-origin"
    >
    </iframe>
    <figcaption>
      Super Cool Video
    </figcaption>
  </figure>
</p>
.

wolframcloud.com iframe
.
@[Hello, World!](https://www.wolframcloud.com/obj/b9e9ecd9-b523-4da8-a7da-948ecfc228a9 "A simple Wolfram Workbook")
.
<p>
  <figure class="markdown-it-embed">
    <iframe
      src="https://www.wolframcloud.com/obj/b9e9ecd9-b523-4da8-a7da-948ecfc228a9"
      title="Hello, World!"
      allowfullscreen
      credentialess
      frameborder="0"
      loading="lazy"
      referrerpolicy="same-origin"
    >
    </iframe>
    <figcaption>
      A simple Wolfram Workbook
    </figcaption>
  </figure>
</p>
.

vimeo.com iframe
.
@[Her Scents of Pu Er](https://player.vimeo.com/video/900211313)
.
<p>
  <figure class="markdown-it-embed">
    <iframe
      src="https://player.vimeo.com/video/900211313"
      title="Her Scents of Pu Er"
      allow="autoplay; fullscreen; picture-in-picture"
      allowfullscreen
      credentialess
      frameborder="0"
      loading="lazy"
      referrerpolicy="same-origin"
    >
    </iframe>
  </figure>
</p>
.

No embed in markdown
.
*other content*
.
<p><em>other content</em></p>
.

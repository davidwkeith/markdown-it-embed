export interface PluginOptions {
  classNames?: string[]
  iframeAttributes?: IframeAttributes,
  useManagedComponents?: boolean,
  domainSpecificConfig?: DomainSpecificConfig[]
}

export interface DomainSpecificConfig {
  domains: string[]
  iframeAttributes?: IframeAttributes
  managedComponentParameters?: Record<string, string>
}

/**
 * Attributes for iFrame embeds.
 */
export interface IframeAttributes {
  allowFullscreen?: boolean
  frameBorder?: 0 | 1
  allow?: string
  classNames?: string[]
  credentialess?: boolean
  csp?: string
  height?: number
  width?: number
  loading?: "eager" | "lazy"
  referrerPolicy?:
    | "no-referrer"
    | "no-referrer-when-downgrade"
    | "origin"
    | "origin-when-cross-origin"
    | "same-origin"
    | "strict-origin"
    | "strict-origin-when-cross-origin"
    | "unsafe-url"
  sandbox?:
    | "allow-downloads"
    | "allow-forms"
    | "allow-modals"
    | "allow-orientation-lock"
    | "allow-pointer-lock"
    | "allow-popups-to-escape-sandbox"
    | "allow-popups"
    | "allow-presentation"
    | "allow-same-origin"
    | "allow-scripts"
    | "allow-storage-access-by-user-activation"
    | "allow-top-navigation"
    | "allow-top-navigation-by-user-activation"
    | "allow-top-navigation-to-custom-protocol"
}
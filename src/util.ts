import type { DomainSpecificConfig, PluginOptions } from "./types"

export function getDomainSpecificConfig(options: PluginOptions, hostname: string): DomainSpecificConfig | null {
  return options.domainSpecificConfig?.find((config: DomainSpecificConfig) => config.domains.includes(hostname)) || null
}
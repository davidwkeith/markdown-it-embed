import type { PluginOptions, DomainSpecificConfig } from "./types"


export const defaultOptions: PluginOptions = {
  classNames: ["markdown-it-embed"],
  useManagedComponents: true,
}


export const defaultDomainSpecificAttributes: DomainSpecificConfig[] = [
  {
    "domains": ["www.youtube.com", "youtube.com", "youtu.be", "youtube-nocookie.com", "www.youtube-nocookie.com"],
    "iframeAttributes": {
      "allow": "accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
    }
  },
  {
    "domains": ["player.vimeo.com", "vimeo.com"],
    "iframeAttributes": {
      "allow": "autoplay; fullscreen; picture-in-picture"
    }
  }
]

export const managedComponents = {  
  instagram: {
    domains: ["www.instagram.com", "instagram.com"],
    component: "instagram-post",
    src: "post-url",
    parameters: {
      
    }
  }
}
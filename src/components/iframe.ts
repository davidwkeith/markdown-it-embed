import type { IframeAttributes, DomainSpecificConfig, PluginOptions } from "../types"
import { getDomainSpecificConfig } from "../util"
import { defaultDomainSpecificAttributes } from "../defaults"

const defaultAttributes: IframeAttributes = {
  allowFullscreen: true,
  frameBorder: 0,
  referrerPolicy: "same-origin",
  loading: "lazy",
  credentialess: true
}

export default {
  domains: [], // fallback for all other domains

  /**
   * Generate HTML for an embedded iframe.
   */
  render(options: PluginOptions, url: URL, title?: string): string {
    const attrs: IframeAttributes = {
      ...defaultAttributes,
      ...defaultDomainSpecificAttributes.find(({ domains }) => domains.includes(url.hostname))?.iframeAttributes,
      ...getDomainSpecificConfig(options, url.hostname)?.iframeAttributes,
    }
  
    const attibutes = [
      attrs.allow ? `allow="${attrs.allow}"` : undefined,
      attrs.allowFullscreen ? "allowfullscreen" : undefined,
      attrs.classNames ? `class="${attrs.classNames.join(" ")}"` : undefined,
      attrs.credentialess ? `credentialess` : undefined,
      `frameborder="${attrs.frameBorder}"`,
      attrs.height ? `height="${attrs.height}"` : undefined,
      `loading="${attrs.loading}"`,
      `referrerpolicy="${attrs.referrerPolicy}"`,
      attrs.sandbox ? `sandbox="${attrs.sandbox}"` : undefined,
      attrs.width ? `width="${attrs.width}"` : undefined,
    ].filter(atrribute => atrribute !== undefined).join(" ") 
  
    return `<iframe src="${url.href}" title="${title}" ${attibutes}></iframe>`
  }
}
import type { PluginOptions } from "../types"
import { getDomainSpecificConfig } from "../util"

const defaultParameters = { 
  captions: true
}

export default {
  domains: ["www.instagram.com", "instagram.com"],

  render(options: PluginOptions, url: URL, title?: string) {

    const params = {
      ...defaultParameters,
      ...getDomainSpecificConfig(options, url.hostname)?.managedComponentParameters,
    }

    const parameters = [
      params.captions ? `captions="true"` : undefined
    ].filter(param => param !== undefined).join(" ") 

    return `<instagram-post post-url="${url.href}" ${parameters}></instagram-post>`
  }
}
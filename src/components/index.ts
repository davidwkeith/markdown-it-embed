import type { PluginOptions } from "../types"
import iframe from "./iframe";
import instagram from "./instagram";
import x from "./x";

const supportedComponents = [instagram, x];

export function supportsManagedComponent(embedDomain: string): boolean {
  return getManagedComponentFor(embedDomain) !== null
}

export function managedComponentHTML(options: PluginOptions, url: URL, title?: string): string {

  const component = getManagedComponentFor(url.hostname);

  const renderer = component ? component.render : iframe.render;

  return renderer(options, url, title);
}

function getManagedComponentFor(embedDomain: string) {
  return supportedComponents.find(({ domains }) => domains.includes(embedDomain)) || null
}
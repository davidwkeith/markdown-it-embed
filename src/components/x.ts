import type { PluginOptions } from "../types"
import { getDomainSpecificConfig } from "../util"

// const defaultParameters = { 

// }

export default {
  domains: ["www.x.com", "x.com", "www.twitter.com", "twitter.com"],

  render(options: PluginOptions, url: URL, title?: string) {

    // const params = {
    //   ...defaultParameters,
    //   ...getDomainSpecificConfig(options, url.hostname)?.managedComponentParameters,
    // }

    // const parameters = [

    // ].filter(param => param !== undefined).join(" ") 

    return `<twitter-post tweet-id="${url.pathname.split('/').pop()}"></twitter-post>`
  }
}